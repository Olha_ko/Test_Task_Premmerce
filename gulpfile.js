var gulp = require("gulp"),
  browserSync = require("browser-sync"),
  minCSS = require("gulp-clean-css"),
  rename = require("gulp-rename"),
  del = require("del"),
  imagemin = require("gulp-imagemin"),
  pngquant = require("imagemin-pngquant"),
  cache = require("gulp-cache"),
  autoprefixer = require("gulp-autoprefixer");

gulp.task("browser-sync", function() {
  browserSync({
    server: {
      baseDir: "app"
    },
    notify: false
  });
});

gulp.task("min-css", function() {
  // css files minimization
  return gulp
    .src("app/css/*.css")
    .pipe(minCSS())
    .pipe(rename({ suffix: ".min" })) //add file extension .min
    .pipe(
      autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
        //add prefixes
        cascade: true
      })
    )
    .pipe(gulp.dest("app/css"));
});

gulp.task("watch", ["browser-sync", "min-css"], function() {
  gulp.watch("app/css/*.css"); //watching css files changes
  gulp.watch("app/*.html", browserSync.reload); //watching html files changes
});

gulp.task("clean", function() {
  return del.sync("dist"); //delete folder
});

gulp.task("img", function() {
  //image compression
  return gulp
    .src("app/img/*")
    .pipe(
      cache(
        imagemin({
          interlaced: true,
          progressive: true,
          svgoPlugins: [{ removeViewBox: false }],
          use: [pngquant()]
        })
      )
    )
    .pipe(gulp.dest("dist/img")); //move the images to a folder
});

gulp.task("build", ["clean", "img"], function() {
  var buildCss = gulp
    .src(["app/css/*.min.css", "app/css/*.css"])
    .pipe(gulp.dest("dist/css")); //move css to a folder

  var buildHtml = gulp.src("app/*.html").pipe(gulp.dest("dist")); //move  html to a folder
});

gulp.task("clear", function() {
  return cache.clearAll(); //clear cache
});

gulp.task("default", ["watch"]);
